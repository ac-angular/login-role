import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { Role } from './core/models/role';
import { AuthGuard } from './core/guards';

const routes: Routes = [{
  path: 'auth',
  component: LayoutComponent,
  loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
  canActivate: [AuthGuard],
  data: { roles: [Role.User, Role.Admin] }
}, {
  path: 'user',
  component: LayoutComponent,
  loadChildren: () => import('./user/user.module').then(m => m.UserModule),
  canActivate: [AuthGuard],
  data: { roles: [Role.User] }
}, {
  path: 'admin',
  component: LayoutComponent,
  loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
  canActivate: [AuthGuard],
  data: { roles: [Role.Admin] }
}, {
  path: 'login',
  component: LoginComponent
}, { 
  path: '', 
  redirectTo: 'login',
  pathMatch: 'full'
}, {
  path: '**',
  redirectTo: 'auth',
  pathMatch: 'full',
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
