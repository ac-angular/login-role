import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './pages/profile/profile.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';

const routes: Routes = [{
  path: 'welcome', 
  component: WelcomeComponent
}, {
  path: 'profile', 
  component: ProfileComponent
}, {
  path: '**',
  redirectTo: 'welcome',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
