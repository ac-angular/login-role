import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { MatCardModule } from '@angular/material/card';
import { ProfileComponent } from './pages/profile/profile.component';
import { SharedModule } from '@app/shared';


@NgModule({
  declarations: [
    WelcomeComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MatCardModule,
    SharedModule
  ]
})
export class AuthModule { }
