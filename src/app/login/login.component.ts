import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isHidePass = true;
  submitted = false;
  error = '';

  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) { 
    this.loginForm = this.createForm();
  }

  ngOnInit(): void {
  }

  createForm() {
    return this.fb.group({
      userCtrl: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
      passCtrl: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]]
    });
  }

  get passwordInput() { 
    return this.loginForm.get('passCtrl'); 
  }
 
  submit() {
    let user = this.loginForm.get("userCtrl")?.value;
    let pass = this.loginForm.get("passCtrl")?.value;
    this.authenticationService.login(user, pass)
    .pipe(first())
    .subscribe({
        next: () => {
            const returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'auth';
            const user = this.authenticationService.userValue;
            this.router.navigateByUrl(returnUrl);
        },
        error: error => {
            this.error = error;
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: this.error,
              showConfirmButton: false,
              timer: 1500
            })
        }
    });  
  }
}
