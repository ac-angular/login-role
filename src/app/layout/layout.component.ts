import { Component, OnInit } from '@angular/core';
import { Role } from '@app/core/models/role';
import { AuthenticationService } from '@app/core/services/authentication.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  user: any;

  constructor(
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.user$.subscribe(x => this.user = x);
   }

  ngOnInit(): void {
  }

  get isAdmin() {
    return this.user && this.user.role === Role.Admin;
  }

  logout() {
    this.authenticationService.logout();
  }

}
