import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  user: any;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const user = this.authenticationService.userValue;
    // si ha un usuario logueado
    if (user) {
      // se controla que el usuario tenga el rol autorizado para acceder a la ruta
      if (route.data.roles && route.data.roles.indexOf(user.role) === -1) {
        // rol no autorizado
        this.router.navigate(['/auth']);
        return false;
      }
      // rol autorizado, se permite la navegacion
      return true;  
    }
    // usuario no logueado. Se redirije a la vista login
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
