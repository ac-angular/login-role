import { Role } from "./role";

export interface User {
    id: number;
    firstName: string;
    lastName: string;
    age: number;
    role: Role;
    token?: string;
}