import { Component, OnInit } from '@angular/core';
import { UserService } from '@app/core/services';

export interface User {
  age: number;
  firstName: string;
  lastName: string;
  role: string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  dataSource: User[] = [];
  displayedColumns: string[] = ['firstName', 'lastName', 'age', 'role'];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.userService.getAll().subscribe(response => {
      this.dataSource = response;
    })
  }
}
